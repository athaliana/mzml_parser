import gzip
import os
import click
from tqdm import tqdm


def openit(out, mode="rt"):
    return gzip.open(out, mode) if out.endswith(".gz") else open(out, mode)


def scanit(
    fname, iterable, mode="rt", desc="scan", lineno="lineno", dynamic_ncols=True
):
    """Scan a possibly gzipped file.

    iterable should be a generator function that takes a FILE object.
    """
    size = os.stat(fname).st_size
    if lineno:
        lineno = click.style(lineno, fg="yellow")

    def update(bar, fpraw, idx):
        if lineno:
            bar.set_postfix({lineno: str(idx + 1)}, refresh=False)
        pos = fpraw.tell()
        if pos > bar.n:
            bar.update(pos - bar.n)
        else:
            bar.refresh()

    with tqdm(
        total=size,
        unit="B",
        postfix={lineno: 0} if lineno else None,
        dynamic_ncols=dynamic_ncols,
        unit_scale=True,
        desc=desc,
    ) as bar:
        if fname.endswith(".gz"):
            with open(fname, "rb") as fpraw, gzip.open(fpraw, mode) as fp:

                for idx, val in enumerate(iterable(fp)):
                    update(bar, fpraw, idx)
                    yield val

        else:
            with open(fname, mode) as fp:

                for idx, val in enumerate(iterable(fp)):
                    update(bar, fp, idx)
                    yield val

