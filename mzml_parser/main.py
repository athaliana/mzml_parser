import csv

from lxml import etree
import click
from .utils import scanit

mz = "http://psi.hupo.org/ms/mzml"
Spectrum = "{%s}spectrum" % mz
mz_nsmap = {"mz": mz}


def parse_mzml(fp):
    def clear(n):
        n.clear()
        while n.getprevious() is not None:
            del n.getparent()[0]

    for action, n in etree.iterparse(fp, events=("end",)):
        if action == "end" and n.tag == Spectrum:
            yield n
            clear(n)


@click.command()
@click.option("-c", "--col", multiple=True)
@click.argument("xmlfiles", nargs=-1, type=click.Path(exists=True, dir_okay=False))
def spectra(xmlfiles, col):
    """parse mzML files."""

    from collections import Counter

    print(col)

    header = ["index"] + list(col)
    for xmlfile in xmlfiles:
        cnt = Counter()
        with open(xmlfile + ".csv", "wt") as fp:
            W = csv.writer(fp)
            W.writerow(header)
            for spectrum in scanit(
                xmlfile, parse_mzml, mode="rb", desc=xmlfile, lineno="spectrum"
            ):
                index = spectrum.attrib["index"]
                d = {}
                for p in spectrum.xpath(".//mz:cvParam", namespaces=mz_nsmap):
                    # code = p.attrib["accession"]
                    name = p.attrib["name"]
                    value = p.attrib["value"]
                    d[name] = value
                    cnt[name] += 1

                if "SIM spectrum" in d:
                    rest = [d.get(c, "") for c in col]
                    W.writerow([index] + rest)
        # click.secho(f"known keys {cnt}", fg="yellow")
