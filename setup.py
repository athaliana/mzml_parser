from setuptools import setup, find_packages

# requirements `pipreqs --force mzml_parser/`
req = [f.strip() for f in open("mzml_parser/requirements.txt")]
# run `pip install --editable .` in this directory
# run `pip uninstall mzml_parser` to uninstall
setup(
    name="mzml_parser",
    version="0.1",
    packages=find_packages(),
    include_package_data=True,
    install_requires=req,
    entry_points="""
        [console_scripts]
        mzml=mzml_parser.main:spectra
    """,
)
